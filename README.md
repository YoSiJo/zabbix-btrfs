# Zabbix btrfs template

## Discreption
Zabbix monitoring scripts, LLD and template for btrfs volumes.
So far the whole thing is only tested on Debian 9.

## Dependence
`btrfs-progs`, `sudo`

## Installation

### Sudoers
Put the `zabbixBtrfs` file from `sudoers.d` directory in the `/etc/suders.d` on your system.

### LLD script
Add the LLD script `lld-btrfs.sh` and `ldd-btrfs-devices.sh` in the `/var/lib/zabbix/ldd` directory

### UserParameters
Add the `UserParameter-btrfs.conf` file to your `zabbix_agent.d` folder.
