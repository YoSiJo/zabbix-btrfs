#!/usr/bin/env bash
set -e

# Script for local installation on server

# Set variables
POSITIONAL=()
zabbix_lld_scrips_path='/var/lib/zabbix/lld'
zabbix_userparameter_source_file='./zabbix_agentd.d/UserParameter-btrfs.conf'
zabbix_script_lld_btrfs_main_source='./scripts/lld-btrfs.sh'
zabbix_script_lld_btrfs_devices_source='./scripts/lld-btrfs-devices.sh'
sudoers_source_file='./sudoers.d/zabbixBtrfs'
verbose=''

# Parse aguments
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    -z|--zabbix-configd-path)
    zabbix_configd_path="$2"
    shift ; shift
    ;;
    -w|--zabbix-userparameter-filename)
    zabbix_userparameter_filename="$2"
    shift ; shift
    ;;
    -s|--sudoersd-path)
    sudoersd_path="$2"
    shift ; shift
    ;;
    -r|--sudoersd-filename)
    sudoersd_filename="$2"
    shift ; shift
    ;;
    -v|--verbose)
    verbose=' -v'
    shift
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

[[ ! -z "${zabbix_configd_path}" ]]           || zabbix_configd_path='/etc/zabbix/zabbix_agentd.conf.d'
[[ ! -z "${zabbix_userparameter_filename}" ]] || zabbix_userparameter_filename='UserParameter-btrfs.conf'
zabbix_userparameter_target_file="${zabbix_configd_path}/${zabbix_userparameter_filename}"

[[ ! -z "${sudoersd_path}" ]]                 || sudoersd_path='/etc/sudoers.d'
[[ ! -z "${sudoersd_filename}" ]]             || sudoersd_filename='zabbixBtrfs'
sudoers_target_file="${sudoersd_path}/${sudoersd_filename}"

mkdir${verbose} -p "${zabbix_configd_path}"
mkdir${verbose} -p "${sudoersd_path}"
mkdir${verbose} -p "${zabbix_lld_scrips_path}"

cp${verbose} "${zabbix_userparameter_source_file}"        "${zabbix_userparameter_target_file}"
cp${verbose} "${sudoers_source_file}"                     "${sudoers_target_file}"
cp${verbose} "${zabbix_script_lld_btrfs_main_source}"     "${zabbix_lld_scrips_path}/"
cp${verbose} "${zabbix_script_lld_btrfs_devices_source}"  "${zabbix_lld_scrips_path}/"

grep -Eq "^Include\s*=\s*${zabbix_configd_path}" /etc/zabbix/zabbix_agentd.conf || echo "Please add this line to your zabbix_agend.conf file: Include=${zabbix_configd_path}/*.conf"
