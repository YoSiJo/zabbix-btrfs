#!/bin/bash

vDeviceUUIDs="$(sudo btrfs filesystem show | grep -Po 'uuid: \K(\S+)')"
vRun=0
vOut='{"data":['

for vUUID in ${vDeviceUUIDs}; do
  vMount="$(findmnt --types btrfs --output UUID,TARGET -r | grep -Po "^${vUUID}\s\K([^\s]+)$" | head -n 1)"
  [[ "${vMount}" = "" ]] && continue

  vIDs="$(sudo btrfs device usage "${vMount}" | grep -Po '(?m)ID:\s+\K(\S+)$')"

  for vID in ${vIDs}; do
    [ ${vRun} != 0 ] && vOut="${vOut},"

    vPath="$(sudo btrfs device usage "${vMount}" | egrep "ID:\s+${vID}$" | grep -Po '^[^,]+')"

    vOut="${vOut}{\"{#BTRFSUUID}\":\"${vUUID}\",\"{#BTRFSMOUNT}\":\"${vMount}\",\"{#BTRFSDEVID}\":\"${vID}\",\"{#BTRFSDEVPATH}\":\"${vPath}\"}"

    vRun=$((${vRun}+1))
  done
done

echo "${vOut}]}"
