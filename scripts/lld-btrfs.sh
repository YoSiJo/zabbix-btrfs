#!/bin/bash

vDeviceUUIDs="$(sudo btrfs filesystem show | grep -Po 'uuid: \K(\S+)')"
vRun=0
vOut='{"data":['

for vUUID in ${vDeviceUUIDs}; do
  vMount="$(findmnt --types btrfs --output UUID,TARGET -r | grep -Po "^${vUUID}\s\K([^\s]+)$" | head -n 1)"
  [[ "${vMount}" = "" ]] && continue

  [ ${vRun} != 0 ] && vOut="${vOut},"

  vOut="${vOut}{\"{#BTRFSUUID}\":\"${vUUID}\",\"{#BTRFSMOUNT}\":\"${vMount}\"}"
  vRun=$((${vRun}+1))
done

echo "${vOut}]}"
